import React, { Component } from "react";
import { Navbar } from "reactstrap";
import { connect } from "react-redux";
import classnames from "classnames";
import axios from "axios";
import { useAuth0 } from "../../../authServices/auth0/auth0Service";
import {
  logoutWithJWT,
  logoutWithFirebase,
} from "../../../redux/actions/auth/loginActions";
import { Redirect } from "react-router-dom";
import NavbarBookmarks from "./NavbarBookmarks";
import NavbarUser from "./NavbarUser";
import userImg from "../../../assets/img/portrait/small/avatar-s-11.jpg";
import { isEmptyObject } from "jquery";
import { FRONTEND_URL, BACKEND_URL } from "../../../constants/Constants";

// const UserName = props => {
//   let username = ""
//   console.log(props.Username)
//   if (props.Username !== undefined) {
//     console.log("")
//     username = props.Username
//   } else if (props.user.login.values !== undefined) {
//     username = props.user.login.values.name
//     if (
//       props.user.login.values.loggedInWith !== undefined &&
//       props.user.login.values.loggedInWith === "jwt"
//     ) {
//       username = props.user.login.values.loggedInUser.name
//     }
//   } else {
//     username = "Pontus Espe"
//   }

//   return username
// }

// const { user } = useAuth0()
// const navbarTypes = ["floating" , "static" , "sticky" , "hidden"]
class ThemeNavbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      location: window.location.href,
      userLoggedin: {},
      user: {},
      navbarTypes: ["floating", "static", "sticky", "hidden"],
      loading: true,
    };
  }

  componentWillMount() {
    axios.defaults.withCredentials = true;
    axios
      .get(BACKEND_URL + "user/me")
      .then((resultUser) => {
        console.log(resultUser.data);
        if (resultUser.data == null) {
          console.log("no data");
          this.setState({ user: {} });
          localStorage.removeItem("user");
          if (window.location.href != FRONTEND_URL + "login") {
            window.location.href = FRONTEND_URL + "login";
          }
        } else {
          if (window.location.href == FRONTEND_URL + "login") {
            window.location.href = FRONTEND_URL;
          }
          localStorage.setItem("user", JSON.stringify(resultUser.data));
          this.setState({ user: resultUser.data });
        }
        this.setState({ loading: false });
      })
      .catch((e) => {
        console.log("no data");
        this.setState({ user: {} });
        localStorage.removeItem("user");
        if (window.location.href != FRONTEND_URL + "login") {
          console.log(e);
          window.location.href = FRONTEND_URL + "login";
        }
      });
  }

  render() {
    const colorsArr = [
      "primary",
      "danger",
      "success",
      "info",
      "warning",
      "dark",
    ];
    const navbarTypes = ["floating", "static", "sticky", "hidden"];
    if (isEmptyObject(this.state.user) && this.state.loading == false) {
      console.log("asdasdasdas");
      return <Redirect to="/login" />;
    } else if (this.state.loading == false) {
      return (
        <React.Fragment>
          <div className="content-overlay" />
          <div className="header-navbar-shadow" />
          <Navbar
            className={classnames(
              "header-navbar navbar-expand-lg navbar navbar-with-menu navbar-shadow",
              {
                "navbar-light": this.props.navbarColor === "default",
                "navbar-dark": "dark",
                "bg-primary":
                  this.props.navbarColor === "primary" &&
                  this.props.navbarType !== "static",
                "bg-danger":
                  this.props.navbarColor === "danger" &&
                  this.props.navbarType !== "static",
                "bg-success":
                  this.props.navbarColor === "success" &&
                  this.props.navbarType !== "static",
                "bg-info":
                  this.props.navbarColor === "info" &&
                  this.props.navbarType !== "static",
                "bg-warning":
                  this.props.navbarColor === "warning" &&
                  this.props.navbarType !== "static",
                "bg-dark":
                  this.props.navbarColor === "dark" &&
                  this.props.navbarType !== "static",
                "d-none":
                  this.props.navbarType === "hidden" && !this.props.horizontal,
                "floating-nav":
                  this.props.navbarType === "floating" &&
                  !this.props.horizontal,
                "navbar-static-top":
                  this.props.navbarType === "static" && !this.props.horizontal,
                "fixed-top":
                  this.props.navbarType === "sticky" || this.props.horizontal,
                scrolling: this.props.horizontal && this.props.scrolling,
              }
            )}
          >
            <div className="navbar-wrapper">
              <div className="navbar-container content">
                <div
                  className="navbar-collapse d-flex justify-content-between align-items-center"
                  id="navbar-mobile"
                >
                  <div className="bookmark-wrapper"></div>
                  {this.props.horizontal ? (
                    <div className="logo d-flex align-items-center">
                      <div className="brand-logo mr-50"></div>
                      <h2 className="text-primary brand-text mb-0">Reelzone</h2>
                    </div>
                  ) : null}
                  <NavbarUser
                    handleAppOverlay={this.props.handleAppOverlay}
                    changeCurrentLang={this.props.changeCurrentLang}
                    userName={this.state.user.Username}
                    userImg={
                      this.props.user.login.values !== undefined &&
                      this.props.user.login.values.loggedInWith !== "jwt" &&
                      this.state.user.AvatarImage
                        ? this.state.user.AvatarImage
                        : this.state.user !== undefined &&
                          this.state.user.AvatarImage
                        ? this.state.user.AvatarImage
                        : "https://cdnb.artstation.com/p/assets/images/images/012/033/999/large/thomas-randby-npe-ahri.jpg?1532668669"
                    }
                    loggedInWith={
                      this.props.user !== undefined &&
                      this.props.user.login.values !== undefined
                        ? this.props.user.login.values.loggedInWith
                        : null
                    }
                    logoutWithJWT={this.props.logoutWithJWT}
                    logoutWithFirebase={this.props.logoutWithFirebase}
                  />
                </div>
              </div>
            </div>
          </Navbar>
        </React.Fragment>
      );
    } else {
      return <div></div>;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth,
  };
};

export default connect(mapStateToProps, {
  logoutWithJWT,
  logoutWithFirebase,
  useAuth0,
})(ThemeNavbar);
