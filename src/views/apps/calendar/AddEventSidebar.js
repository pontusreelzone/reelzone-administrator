import React from "react";
import { X, Tag } from "react-feather";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  FormGroup,
  Input,
  Label,
  Button,
} from "reactstrap";
import Flatpickr from "react-flatpickr";
import { Check } from "react-feather";
import Checkbox from "../../../components/@vuexy/checkbox/CheckboxesVuexy";
import "flatpickr/dist/themes/light.css";
import "../../../assets/scss/plugins/forms/flatpickr/flatpickr.scss";
import Axios from "axios";

const eventColors = {
  business: "chip-success",
  work: "chip-warning",
  personal: "chip-danger",
  others: "chip-primary",
};
class AddEvent extends React.Component {
  state = {
    search: [],
    editPrizepool: false,
    id: "",
    spins: 0,
    games: [],
    start: new Date(),
    retryable: false,
    end: new Date(),
    format: -1,
    image: "",
    title: "",
    description: "",
    attendees: [],
    game: null,
    prizepool: [
      {
        id: "",
        prize: 0,
        position: "",
      },
    ],
  };
  handleDateChange = (selectedDates, dateStr) => {
    this.setState({
      start: selectedDates[0],
    });
  };
  switchFormat = (int) => {
    if (int == 0) {
      return "Bonus";
    }
    if (int == 1) {
      return "Score";
    }
    if (int == 2) {
      return "Multiplier";
    }
  };
  componentDidUpdate = () => {
    this.isFormValid();
    console.log(this.state.search);
  };
  handleendChange = (date) => {
    this.setState({
      end: date[0],
    });
  };

  isFormValid = () => {
    let notFilled = this.state.prizepool.filter(
      (x) => x.position == "" || x.prize == 0 || x.prize == NaN
    );
    if (
      this.state.format == -1 ||
      this.state.title.length == 0 ||
      this.state.description.length == 0 ||
      this.state.game == null ||
      this.state.prizepool.length == 0 ||
      notFilled.length > 0 ||
      this.state.spins == 0
      // this.state.start.getTime() === this.state.end.getTime() ||
      // this.state.start < today ||
      // this.state.start > this.state.end
    ) {
      return true;
    }

    return false;
  };
  handleChangeFormat = (int) => {
    this.setState({
      format: int,
    });
  };

  handleGameChange = (game) => {
    this.setState({
      game: game,
    });
  };
  handleAddPrizepool = () => {
    this.setState({
      prizepool: [...this.state.prizepool, { prize: 0, position: "" }],
    });
  };

  handleRemovePrizepool(index) {
    this.state.prizepool.splice(index, 1);
    this.setState({ prizepool: this.state.prizepool });
  }

  handleAddEvent = () => {
    this.props.handleSidebar(false);

    let success = this.props.addEvent({
      description: this.state.description,
      prizepool: this.state.prizepool,
      image: this.state.image,
      retryable: this.state.retryable,
      spins: this.state.spins,
      title: this.state.title,
      start: this.state.start,
      end: this.state.end,
      game: this.state.game,
      format: this.state.format,
      allDay: this.state.allDay,
      selectable: this.state.selectable,
    });

    this.setState({
      description: "",
      spins: 0,
      prizepool: [
        {
          prize: 0,
          position: "",
        },
      ],
      format: -1,
      retryable: false,
      image: "",
      start: new Date(),
      end: new Date(),
      title: "",
      game: null,
      allDay: true,
      selectable: true,
    });
    this.props.selectedEvent(this.state);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      search: [],
      totalPrizepool: 0,
      editPrizepool: false,
      selectGame: false,
      spins: nextProps.eventInfo === null ? 0 : nextProps.eventInfo.spins,
      retryable:
        nextProps.eventInfo === null ? false : nextProps.eventInfo.retryable,
      format: nextProps.eventInfo === null ? -1 : nextProps.eventInfo.format,
      id: nextProps.eventInfo === null ? "" : nextProps.eventInfo.id,
      games: nextProps.games === null ? "" : nextProps.games,
      title: nextProps.eventInfo === null ? "" : nextProps.eventInfo.title,
      prizepool:
        nextProps.eventInfo === null
          ? [
              {
                id: "",
                prize: 0,
                position: "",
              },
            ]
          : nextProps.eventInfo.prizepool,
      description:
        nextProps.eventInfo === null ? "" : nextProps.eventInfo.description,
      start:
        nextProps.eventInfo === null
          ? new Date()
          : new Date(nextProps.eventInfo.start),
      end:
        nextProps.eventInfo === null
          ? new Date()
          : new Date(nextProps.eventInfo.end),
      game: nextProps.eventInfo === null ? null : nextProps.eventInfo.game,
    });
  }

  render() {
    let events = this.props.events;
    let indexOfEvent = events.findIndex((x) => x.id == this.state.id);
    let total = this.state.prizepool.reduce((a, b) => a + b.prize, 0);
    if (this.state.selectGame) {
      return (
        <div
          style={{ overflow: "auto" }}
          className={`add-event-sidebar ${
            this.props.sidebar ? "show" : "hidden"
          }`}
        >
          <div className="header d-flex justify-content-between">
            <h3>Select Game</h3>
            <div
              className="close-icon cursor-pointer"
              onClick={() => {
                let p = !this.state.selectGame;
                this.setState({
                  selectGame: p,
                });
              }}
            >
              <X size={20} />
            </div>
          </div>
          <br></br>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Button
              color="primary"
              onClick={() => {
                let p = !this.state.selectGame;
                this.setState({
                  selectGame: p,
                });
              }}
            >
              {this.state.game === null ? "None" : this.state.game}
            </Button>
          </div>
          <br></br>
          <FormGroup className="form-label-group">
            <Input
              type="text"
              id="Search Games"
              placeholder="Game"
              onChange={(e) => {
                let searchgames = [];

                this.state.games
                  .filter((game) =>
                    game.toLowerCase().includes(e.target.value.toLowerCase())
                  )
                  .map((filteredGame) => {
                    console.log(filteredGame);
                    searchgames.push(filteredGame);
                  });
                if (searchgames == []) {
                  this.setState({ search: searchgames });
                } else {
                  this.setState({ search: searchgames });
                }
              }}
            />
            <Label for="search">Search</Label>
          </FormGroup>
          {this.state.search.length > 0
            ? this.state.search.map((game) => {
                return (
                  <h1
                    type="Button"
                    style={{
                      color: "#FFB700",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                    onClick={(e) => {
                      this.setState({ game: game });
                    }}
                  >
                    {game}
                  </h1>
                );
              })
            : this.state.games.map((game) => {
                return (
                  <h1
                    type="Button"
                    style={{
                      color: "#FFB700",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                    onClick={(e) => {
                      this.setState({ game: game });
                    }}
                  >
                    {game}
                  </h1>
                );
              })}
        </div>
      );
    }
    if (this.state.editPrizepool) {
      return (
        <div
          style={{ overflow: "auto" }}
          className={`add-event-sidebar ${
            this.props.sidebar ? "show" : "hidden"
          }`}
        >
          <div className="header d-flex justify-content-between">
            <h3 className="text-bold-600 mb-0">Prizepools</h3>
            <div
              className="close-icon cursor-pointer"
              onClick={() => {
                let p = !this.state.editPrizepool;
                this.setState({
                  editPrizepool: p,
                });
              }}
            >
              <X size={20} />
            </div>
          </div>
          <div className="header d-flex justify-content-between">
            <h3 className="text-bold-600 mb-0">Total prize: {total}</h3>
          </div>

          <FormGroup className="form-label-group">
            {this.state.prizepool.map((prizepool, index) => {
              return (
                <div>
                  <FormGroup className="form-label-group">
                    <Input
                      type="number"
                      id="prize"
                      placeholder="Prize"
                      value={prizepool.prize == 0 ? "" : prizepool.prize}
                      onChange={(e) => {
                        const newItems = [...this.state.prizepool];
                        newItems[index].prize = parseInt(e.target.value);
                        this.setState({ prizepool: newItems });
                      }}
                    />
                    <Label for="prize">Prize</Label>
                  </FormGroup>

                  <FormGroup className="form-label-group">
                    <Input
                      type="text"
                      id="placement"
                      placeholder="Position"
                      value={prizepool.position}
                      onChange={(e) => {
                        const newItems = [...this.state.prizepool];
                        newItems[index].position = e.target.value;
                        this.setState({ prizepool: newItems });
                      }}
                    />
                    <Label for="placement">Position</Label>
                    <Button
                      color="btn btn-danger"
                      onClick={() => this.handleRemovePrizepool(index)}
                    >
                      Remove prizepool
                    </Button>
                  </FormGroup>
                </div>
              );
            })}

            <div className="header d-flex justify-content-between">
              <h3 className="text-bold-600 mb-0">Total prize: {total}</h3>
            </div>
            <Label for="PrizePool">Prizepool</Label>
          </FormGroup>
          <Button color="primary" onClick={this.handleAddPrizepool}>
            Create new prizepool
          </Button>
          <Button
            color="flat-danger"
            onClick={() => {
              let p = !this.state.editPrizepool;
              this.setState({
                editPrizepool: p,
              });
            }}
          >
            Close prizepools
          </Button>
        </div>
      );
    } else {
    }
    return (
      <div
        style={{ overflow: "auto" }}
        className={`add-event-sidebar ${
          this.props.sidebar ? "show" : "hidden"
        }`}
      >
        <div className="header d-flex justify-content-between">
          <h3 className="text-bold-600 mb-0">
            {this.props.eventInfo !== null && this.props.eventInfo.id !== ""
              ? "Update Tournament"
              : "Add Tournament"}
          </h3>
          <div
            className="close-icon cursor-pointer"
            onClick={() => this.props.handleSidebar(false)}
          >
            <X size={20} />
          </div>
        </div>
        <div
          style={{
            textAlign: "center",
            display: this.state.image !== "" ? "block" : "none",
          }}
        >
          <img
            src={this.state.image}
            style={{
              width: "100px",
              height: "100px",
            }}
          ></img>
        </div>
        <div className="header d-flex justify-content-between">
          <h3>{this.state.game === null ? "None" : this.state.game}</h3>
          <Button
            color="primary"
            onClick={() => {
              let p = !this.state.selectGame;
              this.setState({
                selectGame: p,
              });
            }}
          >
            Select Game
          </Button>
        </div>

        <div className="add-event-body">
          <div className="category-action d-flex justify-content-between my-50">
            <div className="event-category">
              {this.state.format !== -1 ? (
                <div className={`chip ${eventColors[this.state.format]}`}>
                  <div className="chip-body">
                    <div className="chip-text text-capitalize">
                      {this.switchFormat(this.state.format)}
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
            <div className="category-dropdown">
              <UncontrolledDropdown>
                <DropdownToggle
                  tag="div"
                  className="cursor-pointer"
                  style={{ color: "#FFF" }}
                >
                  Pick Format
                </DropdownToggle>
                <DropdownMenu tag="ul" right>
                  <DropdownItem
                    tag="li"
                    onClick={() => this.handleChangeFormat(0)}
                  >
                    <span>Bonus</span>
                  </DropdownItem>
                  <DropdownItem
                    tag="li"
                    onClick={() => this.handleChangeFormat(1)}
                  >
                    <span>Score</span>
                  </DropdownItem>
                  <DropdownItem
                    tag="li"
                    onClick={() => this.handleChangeFormat(2)}
                  >
                    <span>Multiplier</span>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </div>
          </div>
          <div className="add-event-fields mt-2">
            <FormGroup className="form-label-group">
              <Input
                type="text"
                id="EventTitle"
                placeholder="Title"
                value={this.state.title}
                onChange={(e) => {
                  this.setState({ title: e.target.value });
                }}
              />
              <Label for="EventTitle">Event Title</Label>
            </FormGroup>
            <FormGroup className="form-label-group">
              <Input
                type="text"
                id="description"
                placeholder="Description"
                value={this.state.description}
                onChange={(e) => {
                  this.setState({ description: e.target.value });
                }}
              />
              <Label for="description">Description</Label>
            </FormGroup>
            <div className="header d-flex justify-content-between">
              <h3 className="text-bold-600 mb-0">Total prize: {total}</h3>
              <Button
                color="primary"
                onClick={() => {
                  let p = !this.state.editPrizepool;
                  this.setState({
                    editPrizepool: p,
                  });
                }}
              >
                View Prizepools
              </Button>
            </div>

            {/* <br />
            <br /> */}

            <FormGroup className="form-label-group">
              <Input
                type="number"
                id="Spins"
                defaultValue=""
                placeholder="Spins"
                value={this.state.spins == 0 ? "" : this.state.spins}
                onChange={(e) => {
                  this.setState({ spins: parseInt(e.target.value) });
                }}
              />
              <Label for="Spins">Spins</Label>
            </FormGroup>

            <FormGroup>
              <Label for="start">Start Date</Label>
              <Flatpickr
                id="start"
                className="form-control"
                value={this.state.start}
                onChange={(selectedDates, dateStr) =>
                  this.handleDateChange(selectedDates, dateStr)
                }
                options={{
                  altInput: true,
                  enableTime: true,
                  dateFormat: "Y-m-d H:i",
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label for="end">End Date</Label>
              <Flatpickr
                id="end"
                className="form-control"
                value={this.state.end}
                onChange={(date) => this.handleendChange(date)}
                options={{
                  altInput: true,
                  enableTime: true,
                  dateFormat: "Y-m-d H:i",
                }}
              />
            </FormGroup>
            <FormGroup className="d-flex justify-content-between align-items-center">
              <Checkbox
                id="Remember"
                name="Remember"
                color="primary"
                icon={<Check className="vx-icon" size={16} />}
                label="Retryable"
                checked={this.state.retryable}
                defaultChecked={false}
                onChange={(e) => {
                  this.setState({ retryable: e.target.checked });
                }}
              />
            </FormGroup>
          </div>
          <hr className="my-2" />
          <div className="add-event-actions text-right">
            <Button.Ripple
              disabled={this.isFormValid()}
              color="primary"
              onClick={() => {
                this.props.handleSidebar(false);
                if (
                  this.props.eventInfo !== null &&
                  this.props.eventInfo.id !== ""
                ) {
                  this.props.updateEvent({
                    index: indexOfEvent,
                    id: this.props.eventInfo.id,
                    title: this.state.title,
                    image: this.state.image,
                    retryable: this.state.retryable,
                    description: this.state.description,
                    prizepool: this.state.prizepool,
                    game: this.state.game,
                    spins: this.state.spins,
                    start: this.state.start,
                    end: this.state.end,
                    allDay: true,
                    selectable: true,
                  });
                  this.props.selectedEvent(this.state);
                } else this.handleAddEvent();
              }}
            >
              {this.props.eventInfo !== null &&
              this.props.eventInfo.title.length > 0
                ? "Update Tournament"
                : "Add Tournament"}
            </Button.Ripple>
            {this.props.eventInfo !== null && this.props.eventInfo.id !== "" ? (
              <Button.Ripple
                className="ml-1"
                color="flat-danger"
                onClick={() => {
                  this.props.deleteEvent(this.state.id);
                  this.props.handleSidebar(false);
                }}
              >
                Delete
              </Button.Ripple>
            ) : null}
            <Button.Ripple
              className="ml-1"
              color="flat-danger"
              onClick={() => {
                this.props.handleSidebar(false);
                if (this.props.handleSelectedEvent)
                  this.props.handleSelectedEvent(null);
                else return null;
              }}
            >
              Cancel
            </Button.Ripple>
          </div>
        </div>
      </div>
    );
  }
}

export default AddEvent;
