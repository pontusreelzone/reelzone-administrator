import React from "react";
import {
  Card,
  CardBody,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import classnames from "classnames";
import { User, Info, Share } from "react-feather";
import View from "./EditInf";
import InfoTab from "./View";
import "../../../../assets/scss/pages/users.scss";
class UserEdit extends React.Component {
  static getDerivedStateFromProps(props, state) {
    return {
      user: props.history.location.state.detail,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      user: null,
      activeTab: "1",
    };
  }
  componentDidMount() {
    console.log(this.state.user);
  }
  toggle = (tab) => {
    this.setState({
      activeTab: tab,
    });
  };
  render() {
    return (
      <Row>
        <Col sm="12">
          <Card>
            <CardBody className="pt-2">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "1",
                    })}
                    onClick={() => {
                      this.toggle("1");
                    }}
                  >
                    <User size={16} />
                    <span className="align-middle ml-50">View</span>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "2",
                    })}
                    onClick={() => {
                      this.toggle("2");
                    }}
                  >
                    <Info size={16} />
                    <span className="align-middle ml-50">Edit</span>
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                  <InfoTab user={this.state.user} />
                </TabPane>
                <TabPane tabId="2">
                  <View user={this.state.user} />
                </TabPane>
              </TabContent>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}
export default UserEdit;
