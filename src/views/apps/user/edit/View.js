import React from "react";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Media,
  Row,
  Col,
  Button,
  Table,
} from "reactstrap";
import { Edit, Trash, Lock, Check } from "react-feather";
import { Link } from "react-router-dom";
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import userImg from "../../../../assets/img/portrait/small/avatar-s-18.jpg";
import "../../../../assets/scss/pages/users.scss";
import axios from "axios";
import { FRONTEND_URL, BACKEND_URL } from "../../../../constants/Constants";
class UserInfoTab extends React.Component {
  static getDerivedStateFromProps(props, state) {
    let user = props.user;
    user.birthdate = new Date(props.user.BirthDate);
    user.birthdate =
      user.birthdate.getDate() +
      "." +
      (user.birthdate.getMonth() + 1) +
      "." +
      user.birthdate.getFullYear();
    return {
      user: user,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      user: null,
    };
  }
  displayStatus = () => {};

  render() {
    let user = this.state.user;
    return (
      <React.Fragment>
        <Row>
          <Col sm="12">
            <Card>
              <CardHeader>
                <CardTitle>Account</CardTitle>
              </CardHeader>
              <CardBody>
                <Row className="mx-0" col="12">
                  <Col className="pl-0" sm="12">
                    <Media className="d-sm-flex d-block">
                      <Media className="mt-md-1 mt-0" left>
                        <Media
                          className="rounded mr-2"
                          object
                          src={user.AvatarImage}
                          alt="Generic placeholder image"
                          height="112"
                          width="112"
                        />
                      </Media>
                      <Media body>
                        <Row>
                          <Col sm="15" md="10" lg="10">
                            <div className="users-page-view-table">
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  ID
                                </div>
                                <div>{user.ID}</div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Username
                                </div>
                                <div>{user.Username}</div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Email
                                </div>
                                <div className="text-truncate">
                                  <span>{user.Email}</span>
                                </div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Date of birth
                                </div>
                                <div className="text-truncate">
                                  <span>{user.birthdate}</span>
                                </div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Country
                                </div>
                                <div>{user.Country.Name}</div>
                              </div>
                            </div>

                            <div className="users-page-view-table">
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Status
                                </div>
                                {user.Status == 0
                                  ? "Active"
                                  : user.Status == 1
                                  ? "Banned"
                                  : "Deleted"}
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Role
                                </div>
                                <div>{user.Admin ? "Admin" : "User"}</div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Verified
                                </div>
                                <div>verified</div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Kyc Verified
                                </div>
                                <div>
                                  {user.KycVerification ? "True" : "False"}
                                </div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Level
                                </div>
                                <div>{user.Level}</div>
                              </div>
                              <div className="d-flex user-info">
                                <div className="user-info-title font-weight-bold">
                                  Coins
                                </div>
                                <div>
                                  {user.Coins}
                                  {" R"}
                                </div>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Media>
                    </Media>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default UserInfoTab;
