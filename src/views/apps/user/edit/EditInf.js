import React from "react";
import Flatpickr from "react-flatpickr";

import {
  Media,
  Row,
  Col,
  Button,
  Form,
  Input,
  Label,
  FormGroup,
  Table,
  Alert,
} from "reactstrap";
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import { Check, Lock } from "react-feather";
import Axios from "axios";
import { FRONTEND_URL, BACKEND_URL } from "../../../../constants/Constants";
import "../../../../assets/scss/plugins/forms/flatpickr/flatpickr.scss";
import "flatpickr/dist/themes/light.css";

class UserAccountTab extends React.Component {
  // static getDerivedStateFromProps(props, state) {
  //   let user = props.user;
  //   console.log(user);
  //   user.BirthDate = new Date(props.user.BirthDate);
  //   return {
  //     user: user,
  //   };
  // }
  static getDerivedStateFromProps(props, state) {
    if (JSON.stringify(props.user) !== JSON.stringify(state.user)) {
      let user = props.user;
      console.log(props.user.ID);
      console.log(state.user);

      // console.log(state.user.hasOwnProperty("ID"));

      user.BirthDate = new Date(props.user.BirthDate);
      return {
        user: user,
        changedStatus: user.Status,
      };
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      coins: 0,
      countries: null,
      changedStatus: -1,
      displayMessege: false,
      messege: "",
      user: null,
    };
  }
  async componentDidMount() {
    await Axios.get(BACKEND_URL + "User/countries")
      .then((result) => {
        this.setState({ countries: result.data });
      })
      .catch((e) => {});
  }
  handleUsername = (e) => {
    let user = this.state.user;
    user.Username = e.target.value;
    console.log(this.state.countries);
    this.setState({
      user: user,
    });
  };
  handleCountry = (e) => {
    let user = this.state.user;
    console.log(e.target.value);
    user.Country.Name = e.target.value;
    this.setState({
      user: user,
    });
  };
  handleEmail = (e) => {
    let user = this.state.user;
    user.Email = e.target.value;
    this.setState({
      user: user,
    });
  };
  handleRole = (e) => {
    let user = this.state.user;
    user.Admin = e.target.value;
    this.setState({
      user: user,
    });
  };
  handleKycVerification = (e) => {
    let user = this.state.user;
    if (e.target.value == "true") {
      user.KycVerification = true;
    } else {
      user.KycVerification = false;
    }
    this.setState({
      user: user,
    });
  };
  handleStatus = (e) => {
    let user = this.state.user;
    user.Status = Number(e.target.value);
    console.log(user.Status);
    console.log(this.state.changedStatus);
    if (user.Status == 1 && this.state.changedStatus != 1) {
      this.setState({
        user: user,
        displayMessege: true,
      });
    } else {
      this.setState({
        user: user,
        displayMessege: false,
      });
    }
  };
  handleAdmin = (e) => {
    let user = this.state.user;
    if (e.target.value == "true") {
      user.Admin = true;
    } else {
      user.Admin = false;
    }
    this.setState({
      user: user,
    });
  };
  // handleVerification = (e) => {
  //   let user = this.state.user;
  //   user.Verification = e.target.value;
  //   this.setState({
  //     user: user,
  //   });
  // };
  handledob = (date) => {
    let user = this.state.user;
    user.BirthDate = date[0];
    console.log(date[0]);
    this.setState({
      user: user,
    });
  };
  handleAvatar = () => {
    let user = this.state.user;
    user.AvatarImage =
      "https://res.cloudinary.com/reelzone/image/upload/v1595588889/d8835a090d49a988171ecbf24c107f5c_uf2j0x.png";
    this.setState({
      user: user,
    });
  };
  handleCoins = (e) => {
    let coins = this.state.coins;
    coins = Number(e.target.value);
    this.setState({
      coins: coins,
    });
  };
  handleBanMessege = (e) => {
    this.setState({ messege: e });
  };

  updateUser = (e) => {
    const dateWithTimeZone = (timeZone, date) => {
      let utcDate = new Date(date.toLocaleString("en-US", { timeZone: "UTC" }));
      let tzDate = new Date(
        date.toLocaleString("en-US", { timeZone: timeZone })
      );
      let offset = utcDate.getTime() - tzDate.getTime();
      offset = offset + 14400000;
      date.setTime(date.getTime() + offset);

      return date;
    };
    let dob = dateWithTimeZone(
      "Europe/Stockholm",
      this.state.user.BirthDate
    ).toISOString();

    console.log(this.state);
    e.preventDefault();
    Axios.defaults.withCredentials = true;
    Axios.put(BACKEND_URL + "Admin/user/" + this.state.user.ID, {
      email: this.state.user.Email,
      admin: this.state.user.Admin,
      birthDate: dob,
      username: this.state.user.Username,
      avatarImage: this.state.user.AvatarImage,
      country: this.state.user.Country.Name,
      coins: this.state.coins == null ? 0 : this.state.coins,
      status: this.state.user.Status,
      kycVerification: this.state.user.KycVerification,
    })
      .then((window.location.href = FRONTEND_URL + "app/user/list"))
      .catch((e) => {
        alert("SMTH WENT WRONG");
      });
  };
  render() {
    let user = this.state.user;
    return (
      <Row>
        <Col sm="12">
          <Media className="mb-2">
            <Media className="mr-2 my-25" left>
              <Media
                className="users-avatar-shadow rounded"
                object
                src={user.AvatarImage}
                alt="user profile image"
                height="84"
                width="84"
              />
            </Media>
            <Media className="mt-2" body>
              <Media className="font-medium-1 text-bold-600" tag="p" heading>
                {user.Username}
              </Media>
              <div className="d-flex flex-wrap">
                <Button.Ripple className="mr-1" color="primary" outline>
                  Change
                </Button.Ripple>
                <Button on onClick={this.handleAvatar} color="flat-danger">
                  Remove Avatar
                </Button>
              </div>
            </Media>
          </Media>
        </Col>
        <Col sm="12">
          <Form onSubmit={(e) => this.updateUser(e)}>
            <Row>
              <Col md="6" sm="12">
                <FormGroup>
                  <Label for="username">Username</Label>
                  <Input
                    type="text"
                    value={user.Username}
                    onChange={(e) => this.handleUsername(e)}
                    id="username"
                    placeholder="Username"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Input
                    type="text"
                    value={user.Email}
                    id="email"
                    placeholder="Email"
                    onChange={(e) => this.handleEmail(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <Label className="d-block" for="dob">
                    Date of birth
                  </Label>
                  <Flatpickr
                    id="dob"
                    className="form-control"
                    options={{ dateFormat: "Y-m-d" }}
                    value={user.BirthDate}
                    onChange={(date) => this.handledob(date)}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="Country">Country</Label>
                  <Input
                    type="select"
                    value={user.Country.ID}
                    id="Country"
                    placeholder="Country"
                    onChange={(e) => this.handleCountry(e)}
                  >
                    {this.state.countries == null
                      ? null
                      : this.state.countries.map((country) => {
                          return (
                            <option value={country.id}>{country.name}</option>
                          );
                        })}
                  </Input>
                </FormGroup>
              </Col>
              <Col md="6" sm="12">
                <FormGroup>
                  <Label for="status">Status</Label>
                  <Input
                    type="select"
                    name="status"
                    id="status"
                    value={user.Status}
                    onChange={(e) => this.handleStatus(e)}
                  >
                    <option value="0">Active</option>
                    <option value="1">Banned</option>
                    <option value="2">Deleted</option>
                  </Input>
                </FormGroup>
                {this.state.displayMessege ? (
                  <FormGroup>
                    <Label for="Banned">Ban Messege</Label>
                    <Input
                      type="text"
                      value={this.state.messege}
                      id="Banned"
                      placeholder="Messege"
                      onChange={(e) => this.handleBanMessege(e)}
                    />
                  </FormGroup>
                ) : null}
                <FormGroup>
                  <Label for="role">Role</Label>
                  <Input
                    type="select"
                    name="role"
                    id="role"
                    value={user.Admin}
                    onChange={(e) => this.handleAdmin(e)}
                  >
                    <option value={false}>User</option>
                    <option value={true}>Admin</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="Coins">Coins</Label>
                  <Input
                    type="number"
                    value={this.state.coins == 0 ? null : this.state.coins}
                    id="Coins"
                    placeholder="Coins"
                    onChange={(e) => this.handleCoins(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="KycVerification">Kyc Verification</Label>
                  <Input
                    type="select"
                    name="KycVerification"
                    id="KycVerification"
                    value={user.KycVerification}
                    onChange={(e) => this.handleKycVerification(e)}
                  >
                    <option value={true}>True</option>
                    <option value={false}>False</option>
                  </Input>
                </FormGroup>

                <Button.Ripple type="submit" className="mr-1" color="primary">
                  Save Changes
                </Button.Ripple>
                <Button.Ripple color="flat-warning">Reset</Button.Ripple>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    );
  }
}
export default UserAccountTab;
