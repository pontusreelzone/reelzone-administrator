import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  FormGroup,
  Label,
  Input,
  Row,
  Col,
  UncontrolledDropdown,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Collapse,
  Spinner,
  Button,
} from "reactstrap";
import axios from "axios";
import { ContextLayout } from "../../../../utility/context/Layout";
import { AgGridReact } from "ag-grid-react";
import {
  Edit,
  Trash2,
  ChevronDown,
  Clipboard,
  Printer,
  Download,
  RotateCw,
  X,
} from "react-feather";
import classnames from "classnames";
import { history } from "../../../../history";
import "../../../../assets/scss/plugins/tables/_agGridStyleOverride.scss";
import "../../../../assets/scss/pages/users.scss";
import { BACKEND_URL } from "../../../../constants/Constants";
import { ajax } from "jquery";
class UsersList extends React.Component {
  state = {
    disableButton: false,
    currentPage: 1,
    rowData: null,
    pageSize: 20,
    isVisible: true,
    reload: false,
    collapse: true,
    status: "Opened",
    role: "All",
    selectStatus: "All",
    verified: "All",
    department: "All",
    defaultColDef: {
      sortable: true,
    },
    searchVal: "",
    columnDefs: [
      {
        headerName: "ID",
        field: "ID",
        width: 150,
        filter: false,
        checkboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
        headerCheckboxSelection: true,
      },
      {
        headerName: "Username",
        field: "Username",
        filter: "agTextColumnFilter",
        width: 250,
        cellRendererFramework: (params) => {
          return (
            <div
              className="d-flex align-items-center cursor-pointer"
              onClick={() =>
                history.push({
                  pathname: "/app/user/edit",
                  state: { detail: params.data },
                })
              }
            >
              <img
                className="rounded-circle mr-50"
                src={params.data.AvatarImage}
                alt="user avatar"
                height="30"
                width="30"
              />
              <span>{params.data.Username}</span>
            </div>
          );
        },
      },
      {
        headerName: "Email",
        field: "Email",
        filter: true,
        width: 250,
        cellRendererFramework: (params) => {
          return (
            <div
              className="d-flex align-items-center cursor-pointer"
              onClick={() =>
                history.push({
                  pathname: "/app/user/edit",
                  state: { detail: params.data },
                })
              }
            >
              {" "}
              <span>{params.data.Email}</span>
            </div>
          );
        },
      },
      {
        headerName: "BirthDate",
        field: "BirthDate",
        width: 200,
        filter: true,
        filter: "agDateColumnFilter",
        cellRendererFramework: (params) => {
          var cellDate = new Date(params.data.BirthDate);
          var renderDate =
            cellDate.getDate() +
            "." +
            (cellDate.getMonth() + 1) +
            "." +
            cellDate.getFullYear();
          return <div>{renderDate}</div>;
        },
        filterParams: {
          comparator: function (filterLocalDateAtMidnight, cellValue) {
            var dateAsString = cellValue;
            if (dateAsString == null) return -1;
            var dateParts = dateAsString.split("/");
            var cellDate = new Date(
              Number(dateParts[2]),
              Number(dateParts[1]) - 1,
              Number(dateParts[0])
            );
            if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
              return 0;
            }
            if (cellDate < filterLocalDateAtMidnight) {
              return -1;
            }
            if (cellDate > filterLocalDateAtMidnight) {
              return 1;
            }
          },
          browserDatePicker: true,
        },
      },
      {
        headerName: "KYC Verification",
        field: "KycVerification",
        sortable: false,
        filter: true,
        width: 200,
      },
      {
        headerName: "Coins",
        field: "Coins",
        filter: "agNumberColumnFilter",
        width: 150,
      },
      {
        headerName: "Level",
        field: "Level",
        filter: true,
        width: 150,
      },
      // {
      //   headerName: "Name",
      //   field: "Username",
      //   filter: true,
      //   width: 200,
      // },
      {
        headerName: "Country",
        field: "Country.Name",
        filter: true,
        width: 200,
      },
      {
        headerName: "Admin",
        field: "Admin",
        filter: true,
        width: 150,
      },
      {
        headerName: "Status",
        field: "Status",
        filter: true,
        width: 150,
        cellRendererFramework: (params) => {
          if (params.data.Status === 0) {
            return <div>Active</div>;
          }
          if (params.data.Status === 1) {
            return <div>Banned</div>;
          }
          if (params.data.Status === 2) {
            return <div>Deleted</div>;
          }
        },
      },
      {
        headerName: "Verified",
        field: "is_verified",
        filter: true,
        width: 125,
        cellRendererFramework: (params) => {
          return params.value === true ? (
            <div className="bullet bullet-sm bullet-primary"></div>
          ) : params.value === false ? (
            <div className="bullet bullet-sm bullet-secondary"></div>
          ) : null;
        },
      },
    ],
  };
  async componentDidMount() {
    axios.defaults.withCredentials = true;
    await axios
      .get(
        BACKEND_URL +
          "Admin/users/" +
          this.state.pageSize +
          "/" +
          this.state.currentPage
      )
      .then((response) => {
        let rowData = response.data;

        this.setState({ rowData: rowData });
      })
      .catch((e) => {});
  }

  onGridReady = (params) => {
    this.gridApi = params.api;

    // this.gridApi.onFilterChanged();

    this.gridColumnApi = params.columnApi;
  };

  filterUsers = (page) => {
    var filters = [];
    var userName = this.gridApi.getFilterInstance("Username").appliedModel;
    if (userName !== null && userName !== undefined) {
      userName.name = "username";
      // userName.Filter = userName.Filter.toLowerCase();
      filters.push(userName);
    }
    var email = this.gridApi.getFilterInstance("Email").appliedModel;
    if (email !== null && email !== undefined) {
      email.name = "email";
      // email.Filter = email.Filter.toLowerCase();
      filters.push(email);
    }
    // var coins = this.gridApi.getFilterInstance("Coins").appliedModel;
    // if (coins !== null && coins !== undefined) {
    //   coins.name = "coins";
    //   filters.push(coins);
    // }
    var level = this.gridApi.getFilterInstance("Level").appliedModel;
    if (level !== null && level !== undefined) {
      level.name = "level";
      // level.Filter = level.Filter.toLowerCase();
      filters.push(level);
    }
    var status = this.gridApi.getFilterInstance("Status").appliedModel;
    if (status !== null && status !== undefined) {
      status.name = "status";
      // status.Filter = status.Filter.toLowerCase();
      filters.push(status);
    }
    var country = this.gridApi.getFilterInstance("Country.Name").appliedModel;
    if (country !== null && country !== undefined) {
      country.name = "country";
      // country.Filter = country.Filter.toLowerCase();
      filters.push(country);
    }
    var dob = this.gridApi.getFilterInstance("BirthDate");
    if (
      dob.appliedModel !== null &&
      dob.appliedModel !== undefined &&
      dob.hasOwnProperty("appliedModel")
    ) {
      dob = dob.appliedModel;
      dob.Filter = dob.dateFrom;

      dob.name = "dob";
      filters.push(dob);
    }
    var verified = this.gridApi.getFilterInstance("is_verified").appliedModel;
    if (verified !== null && verified !== undefined) {
      verified.name = "verified";
      // verified.Filter = verified.Filter.toLowerCase();
      filters.push(verified);
    }
    var kycVerified = this.gridApi.getFilterInstance("KycVerification")
      .appliedModel;
    if (kycVerified !== null && kycVerified !== undefined) {
      kycVerified.name = "kycverified";
      // kycVerified.Filter = kycVerified.Filter.toLowerCase();
      filters.push(kycVerified);
    }
    var admin = this.gridApi.getFilterInstance("Admin").appliedModel;
    if (admin !== null && admin !== undefined) {
      admin.name = "admin";
      // admin.Filter = admin.Filter.toLowerCase();
      filters.push(admin);
    }

    axios
      .post(
        BACKEND_URL + "Admin/users/filter/" + this.state.pageSize + "/" + page,
        {
          filters: filters,
        }
      )
      .then((response) => {
        let rowData = response.data;
        console.log(rowData);
        if (rowData.length === 0) {
          this.setState({ disableButton: true });
        } else {
          this.setState({
            rowData: rowData,
            currentPage: page,
            disableButton: false,
          });
        }
      })
      .catch((e) => {});
  };

  filterSize = (val) => {
    if (this.gridApi) {
      this.gridApi.paginationSetPageSize(Number(val));
      this.setState({
        pageSize: val,
      });
    }
  };

  render() {
    const { rowData, columnDefs, defaultColDef, pageSize } = this.state;
    return (
      <Row className="app-user-list">
        <Col sm="12">
          <Card>
            <CardBody>
              <div className="ag-theme-material ag-grid-table">
                <div className="ag-grid-actions d-flex justify-content-between flex-wrap mb-1">
                  <div className="sort-dropdown">
                    <UncontrolledDropdown className="ag-dropdown p-1">
                      <DropdownToggle tag="div">
                        1 - {pageSize} of 150
                        <ChevronDown className="ml-50" size={15} />
                      </DropdownToggle>
                      <DropdownMenu right>
                        <DropdownItem
                          tag="div"
                          onClick={() => this.filterSize(20)}
                        >
                          20
                        </DropdownItem>
                        <DropdownItem
                          tag="div"
                          onClick={() => this.filterSize(50)}
                        >
                          50
                        </DropdownItem>
                        <DropdownItem
                          tag="div"
                          onClick={() => this.filterSize(100)}
                        >
                          100
                        </DropdownItem>
                        <DropdownItem
                          tag="div"
                          onClick={() => this.filterSize(150)}
                        >
                          150
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>
                  <div className="filter-actions d-flex">
                    <Button
                      className="px-2 py-75"
                      color="white"
                      onClick={(e) => {
                        this.setState({ disableButton: false });
                        this.filterUsers(1);
                      }}
                    >
                      Apply silters
                    </Button>
                  </div>
                </div>

                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    color: "white",
                  }}
                >
                  <Button
                    color="primary"
                    disabled={this.state.currentPage == 1 ? true : false}
                    onClick={(e) => {
                      this.filterUsers(this.state.currentPage - 1);
                    }}
                  >
                    {"<"}
                  </Button>
                  <div> Page {this.state.currentPage} </div>
                  <Button
                    disabled={this.state.disableButton}
                    color="primary"
                    onClick={(e) => {
                      this.filterUsers(this.state.currentPage + 1);
                    }}
                  >
                    {">"}
                  </Button>
                </div>
                {this.state.rowData !== null ? (
                  <ContextLayout.Consumer>
                    {(context) => (
                      <AgGridReact
                        rowSelection="multiple"
                        defaultColDef={defaultColDef}
                        columnDefs={columnDefs}
                        rowData={rowData}
                        onGridReady={this.onGridReady}
                        colResizeDefault={"shift"}
                        animateRows={true}
                        floatingFilter={true}
                        // rowModelType={this.state.rowModelType}
                        pagination={true}
                        pivotPanelShow="always"
                        paginationPageSize={pageSize}
                        resizable={true}
                        enableRtl={context.state.direction === "rtl"}
                      />
                    )}
                  </ContextLayout.Consumer>
                ) : null}
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}
const dataSource = {};
export default UsersList;
