import { Formik } from "formik";
import * as EmailValidator from "email-validator";
import * as Yup from "yup";

// import React,{useState} from 'react'
import axios from "axios";
import React from "react";
import { CardBody, FormGroup, Form, Input, Button, Label } from "reactstrap";
import { Mail, Lock, Check } from "react-feather";
import { Link } from "react-router-dom";
import Checkbox from "../../../components/@vuexy/checkbox/CheckboxesVuexy";
import classnames from "classnames";
import loginImg from "../../../assets/img/pages/login.png";
import { FRONTEND_URL, BACKEND_URL } from "../../../constants/Constants";
//   import "../../../../assets/scss/pages/authentication.scss"

const tryLogin = () => (
  <Formik
    initialValues={{ email: "", password: "", remember: false }}
    // handleRemember = e =>{
    //     this.setState(prevState =>({remember: !prevState.remember}));
    // }

    onSubmit={(values, { setSubmitting, setErrors }) => {
      axios.defaults.withCredentials = true;
      setSubmitting(true);
      axios
        .post(BACKEND_URL + "Admin/signin", {
          Email: values.email,
          Password: values.password,
          RememberMe: values.remember,
        })
        .then((result) => {
          if (result.data.successful === true) {
            axios
              .get(BACKEND_URL + "user/me")
              .then((resultUser) => {
                localStorage.setItem("user", JSON.stringify(resultUser.data));
                window.location.href = FRONTEND_URL;
              })
              .catch((e) => {
                if (window.location.href != FRONTEND_URL + "login") {
                  console.log(e);
                  window.location.href = FRONTEND_URL + "login";
                }
              });
          }
        })
        .catch((error) => {
          setSubmitting(false);
          if (error.response) {
            setErrors({ email: "Wrong email or password" });
            if (window.location.href != FRONTEND_URL + "login") {
              console.log(error);
              window.location.href = FRONTEND_URL + "login";
            }
          }
        });
    }}
    validationSchema={Yup.object().shape({
      email: Yup.string().email().required("Required"),
      password: Yup.string()
        .required("No password provided.")
        .matches(
          /^(?!.* )(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
          "Password must contain at least one digit,at least one lower case,at least one upper case, and at least one symbol"
        ),
    })}
  >
    {(props) => {
      const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
      } = props;

      return (
        <React.Fragment>
          <CardBody className="pt-1">
            <Form action="/" onSubmit={handleSubmit}>
              <FormGroup className="form-label-group position-relative has-icon-left">
                <Input
                  id="email"
                  name="email"
                  type="email"
                  placeholder="Email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.email && touched.email && "error"}
                />
                <div className="form-control-position">
                  <Mail size={15} />
                </div>
                <Label>Email</Label>
              </FormGroup>
              {errors.email && touched.email && (
                <div style={{ color: "red" }}>
                  {errors.email}
                  <br />
                  <br />
                </div>
              )}
              <FormGroup className="form-label-group position-relative has-icon-left">
                <Input
                  id="password"
                  name="password"
                  type="password"
                  placeholder="Password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.password && touched.password && "error"}
                />
                <div className="form-control-position">
                  <Lock size={15} />
                </div>
                <Label>Password</Label>
              </FormGroup>
              {errors.password && touched.password && (
                <div style={{ color: "red" }}>{errors.password}</div>
              )}
              <FormGroup className="d-flex justify-content-between align-items-center">
                <Checkbox
                  id="Remember"
                  name="Remember"
                  color="primary"
                  icon={<Check className="vx-icon" size={16} />}
                  label="Remember me"
                  defaultChecked={false}
                  onChange={handleChange}
                />
                <div className="float-right">
                  <Link to="/../pages/forgot-password">Forgot Password?</Link>
                </div>
              </FormGroup>
              <div className="d-flex justify-content-between">
                <Button.Ripple
                  color="primary"
                  type="submit"
                  disabled={
                    isSubmitting ||
                    errors.email ||
                    errors.password ||
                    !touched.email
                  }
                >
                  Login
                </Button.Ripple>
              </div>
            </Form>
          </CardBody>
        </React.Fragment>
      );
    }}
  </Formik>
);

export default tryLogin;
