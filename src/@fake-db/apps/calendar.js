import mock from "../mock"
let data = {
  events: [
    {
      id: 1,
      title: "Reactoonz €1500",
      start: new Date(),
      end: new Date(),
      label: "Reactoonz",
      allDay: true,
      selectable: true
    }
  ]
}

// GET : Calendar Events
mock.onGet("/api/apps/calendar/events").reply(() => {
  return [200, data.events]
})
