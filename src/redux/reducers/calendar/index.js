import update from "react-addons-update";
const initialState = {
  events: [],
  games: [],
  sidebar: false,
  selectedEvent: null,
  error: "",
  loading: false,
};

const calenderReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_EVENTS":
      return { ...state, events: action.events };
    case "FETCH_GAMES":
      return { ...state, games: action.events };

    case "ADD_EVENT":
      return { ...state, events: [...state.events, action.event] };
    case "DELETE_EVENT":
      return {
        ...state,
        events: [...state.events.filter((x) => x.id !== action.id)],
      };
    case "UPDATE_EVENT":
      return update(state, {
        events: { [action.event.index]: { $set: action.event } },
      });
    case "HANDLE_LOADING":
      return { ...state, loading: action.loading };
    case "ERROR_HANDLE":
      return { ...state, error: action.error };
    case "HANDLE_SIDEBAR":
      return { ...state, sidebar: action.status };
    case "HANDLE_SELECTED_EVENT":
      return { ...state, selectedEvent: action.event };
    default:
      return state;
  }
};

export default calenderReducer;
