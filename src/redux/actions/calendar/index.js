import axios from "axios";
import { FRONTEND_URL, BACKEND_URL } from "../../../constants/Constants";
import { element } from "prop-types";
axios.defaults.withCredentials = true;
export const fetchGames = () => {
  let games = [];
  return async (dispatch) => {
    dispatch({ type: "HANDLE_LOADING", loading: true });
    dispatch({ type: "ERROR_HANDLE", error: "" });

    await axios
      .get(BACKEND_URL + "games")
      .then((response) => {
        console.log(response);
        response.data.forEach((element) => {
          games.push(element);
        });
        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({ type: "FETCH_GAMES", events: games });
      })
      .catch((e) => {
        let error = e.response.data.message;
        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({
          type: "ERROR_HANDLE",
          error: error || "Something went wrong.",
        });
      });
    console.log(games);
  };
};
export const fetchEvents = () => {
  let eventList = [];
  let attendes = [];
  let prizepool = [];

  return async (dispatch) => {
    dispatch({ type: "HANDLE_LOADING", loading: true });
    dispatch({ type: "ERROR_HANDLE", error: "" });

    await axios
      .get(BACKEND_URL + "Tournament/all")
      .then((response) => {
        response.data.forEach((element) => {
          prizepool = [];
          element.Prizepool.forEach((prizepoolElement) => {
            prizepool.push({
              id: prizepoolElement.ID,
              prize: prizepoolElement.Prize,
              position: prizepoolElement.Position,
            });

            element.Attendes.forEach((attendee) => {
              attendes.push({});
            });
          });

          const convElement = {
            image: element.HeaderImage,
            id: element.ID,
            retryable: element.Retryable,
            spins: element.StartSpins,
            format: element.Format,
            title: element.Name,
            game: element.Game.Name,
            start: element.StartDOT,
            end: element.EndDOT,
            prizepool: prizepool,
            attendes: element.Attendes,
            description: element.Description,
            image: element.HeaderImage,
          };

          eventList.push(convElement);
        });
        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({ type: "FETCH_EVENTS", events: eventList });
      })
      .catch((e) => {
        let error = e.response.data.message;

        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({
          type: "ERROR_HANDLE",
          error: error || "Something went wrong.",
        });
      });
  };
};
export const deleteEvent = (id) => {
  axios.defaults.withCredentials = true;
  return async (dispatch) => {
    dispatch({ type: "HANDLE_LOADING", loading: true });
    dispatch({ type: "ERROR_HANDLE", error: "" });

    await axios
      .delete(BACKEND_URL + "tournaments/" + id)
      .then((responce) => {
        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({ type: "DELETE_EVENT", id: id });
      })
      .catch((e) => {
        let error = e.response.data.message;

        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({
          type: "ERROR_HANDLE",
          error: error || "Something went wrong.",
        });
      });
  };
};
export const handleSidebar = (bool) => {
  return (dispatch) => dispatch({ type: "HANDLE_SIDEBAR", status: bool });
};

const dateWithTimeZone = (timeZone, date) => {
  let utcDate = new Date(date.toLocaleString("en-US", { timeZone: "UTC" }));
  let tzDate = new Date(date.toLocaleString("en-US", { timeZone: timeZone }));
  let offset = utcDate.getTime() - tzDate.getTime();
  offset = offset + 14400000;
  date.setTime(date.getTime() + offset);

  return date;
};
export const clearError = () => {
  return (dispatch) => {
    dispatch({ type: "ERROR_HANDLE", error: "" });
  };
};
export const addEvent = (event) => {
  return (dispatch) => {
    dispatch({ type: "HANDLE_LOADING", loading: true });
    dispatch({ type: "ERROR_HANDLE", error: "" });

    axios
      .post(BACKEND_URL + "Admin/tournaments", {
        name: event.title,
        description: event.description,
        headerImage: event.image,
        retryable: event.retryable,
        startDOT: dateWithTimeZone(
          "Europe/Stockholm",
          event.start
        ).toISOString(),
        endDOT: dateWithTimeZone("Europe/Stockholm", event.end).toISOString(),
        prizepool: event.prizepool,
        startSpins: event.spins,
        game: event.game,
        format: event.format,
      })
      .then((response) => {
        let prizepool = [];
        response.data.Prizepool.forEach((prizepoolElement) => {
          prizepool.push({
            id: prizepoolElement.ID,
            prize: prizepoolElement.Prize,
            position: prizepoolElement.Position,
          });

          // response.data.Attendes.forEach((attendee) => {
          //   attendes.push({});
          // });
        });

        const convElement = {
          image: response.data.HeaderImage,
          id: response.data.ID,
          retryable: response.data.Retryable,
          spins: response.data.StartSpins,
          format: response.data.Format,
          title: response.data.Name,
          game: response.data.Game.Name,
          start: response.data.StartDOT,
          end: response.data.EndDOT,
          prizepool: prizepool,
          attendes: response.data.Attendes,
          description: response.data.Description,
          image: response.data.HeaderImage,
        };
        dispatch({ type: "ADD_EVENT", event: convElement });
        dispatch({ type: "HANDLE_LOADING", loading: false });
      })
      .catch((e) => {
        let error = e.response.data.message;
        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({
          type: "ERROR_HANDLE",
          error: error || "Something went wrong.",
        });
      });
  };
};
export const updateEvent = (event) => {
  return (dispatch) => {
    dispatch({ type: "HANDLE_LOADING", loading: true });
    dispatch({ type: "ERROR_HANDLE", error: "" });

    axios
      .put(BACKEND_URL + "tournaments/" + event.id, {
        name: event.title,
        description: event.description,
        headerImage: event.image,
        startDOT: dateWithTimeZone(
          "Europe/Stockholm",
          event.start
        ).toISOString(),
        retryable: event.retryable,
        startSpins: event.spins,
        endDOT: dateWithTimeZone("Europe/Stockholm", event.end).toISOString(),
        prizepool: event.prizepool,
        game: event.game,
        format: event.format,
      })
      .then((response) => {
        dispatch({ type: "UPDATE_EVENT", event: event });
        dispatch({ type: "HANDLE_LOADING", loading: false });
      })
      .catch((e) => {
        let error = e.response.data.message;

        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({
          type: "ERROR_HANDLE",
          error: error || "Something went wrong.",
        });
      });
  };
};

export const updateDrag = (event) => {
  return (dispatch) => {
    dispatch({ type: "HANDLE_LOADING", loading: true });
    dispatch({ type: "ERROR_HANDLE", error: "" });

    axios
      .put(BACKEND_URL + "tournaments/" + event.id, {
        name: event.title,
        description: event.description,
        headerImage: event.image,
        startDOT: dateWithTimeZone(
          "Europe/Stockholm",
          event.start
        ).toISOString(),
        retryable: event.retryable,
        startSpins: event.spins,
        endDOT: dateWithTimeZone("Europe/Stockholm", event.end).toISOString(),
        prizepool: event.prizepool,
        game: event.game,
        format: event.format,
      })
      .then((response) => {
        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({ type: "UPDATE_DRAG", event: event });
      })
      .catch((e) => {
        let error = e.response.data.message;

        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({
          type: "ERROR_HANDLE",
          error: error || "Something went wrong.",
        });
      });
  };
};

export const updateResize = (event) => {
  return (dispatch) => {
    dispatch({ type: "HANDLE_LOADING", loading: true });
    dispatch({ type: "ERROR_HANDLE", error: "" });
    axios
      .put(BACKEND_URL + "tournaments/" + event.id, {
        name: event.title,
        description: event.description,
        headerImage: event.image,
        startDOT: dateWithTimeZone(
          "Europe/Stockholm",
          event.start
        ).toISOString(),
        retryable: event.retryable,
        startSpins: event.spins,
        endDOT: dateWithTimeZone("Europe/Stockholm", event.end).toISOString(),
        prizepool: event.prizepool,
        game: event.game,
        format: event.format,
      })
      .then((response) => {
        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({ type: "EVENT_RESIZE", event: event });
      })
      .catch((e) => {
        let error = e.response.data.message;

        dispatch({ type: "HANDLE_LOADING", loading: false });
        dispatch({
          type: "ERROR_HANDLE",
          error: error || "Something went wrong.",
        });
      });
  };
};

export const handleSelectedEvent = (event) => {
  return (dispatch) => dispatch({ type: "HANDLE_SELECTED_EVENT", event });
};
